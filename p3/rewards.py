import numpy as np
from pprint import pprint

def compute_deaths(player):


    return float((player[-1].action_state.value <= 0xA) and (player[-4].action_state.value > 0xA))

    # a = min(player[i].action_state.value for i in range(15, 20))
    # b = max(player[i].action_state.value for i in range(0, 5))
    #
    # return float(a <= 0xA < b)

  # deaths = np.logical_and(np.logical_not(dead[:-1]), dead[1:])


def isDead(player):
    # working ??
    return player.action_state.value <= 0xA

def isDone(state):

    return (isDead(state.players[0]) or isDead(state.players[2]))


def compute_damages(player):
  # percents = []
  # for state in player:
  #     percents.append(state.percent)
  #
  # percents = np.array(percents)
  # res = np.maximum(percents[1:] - percents[:-1], 0.0)

  return float(max([player[-1].percent - player[0].percent, 0]))

  # percents = player['percent']
  # damages = np.maximum(percents[1:] - percents[:-1], 0)
  # return damages


def compute_rewards(states, life_span=0.0, enemies=[0], allies=[2], damage_ratio=0.01, distance_ratio=0.0005, loss_intensity=1.0,
                    velocity_ratio=0.003, life_span_ratio=1.0/3600, recovery_ratio=0.1):
    """Computes rewards from a list of state transitions.

    Args:
      states: current states
      life_span: how long has it been alive
      enemies: The list of pids on the enemy team.
      allies: The list of pids on our team.
      damage_ratio: How much damage (percent) counts relative to stocks.
      distance_ratio: How much distance counts
      loss_intensity: does loosing suck ?
      velocity_ratio: make if flashy
      life_span_ratio: suriving 1 minute = 1 point
      recovery_ratio: reward if goes up when low on y axis
    Returns:
      A length T numpy array with the rewards on each transition.
    """

    pids = enemies + allies

    players = [ [], [], [], []]
    for enum, _ in enumerate(players):
        if enum in pids:
            players[enum] = [state.players[enum] for state in states]

    # players = states['players'] -70.3 -11.75 -110

    # print(players[0][-1].pos_y)

    deaths = {p: compute_deaths(players[p]) for p in pids}
    damages = {p: compute_damages(players[p]) for p in pids}
    losses = {p: 0*deaths[p] + damage_ratio * damages[p] for p in pids}
    distance_rwd = (distance(states[-1]) - distance(states[0])) * distance_ratio
    velocity = velocity_ratio * abs(players[2][-1].speed_ground_x_self/3)
    surviving_point = min([1, life_span * life_span_ratio])
    falling_loss = falling_penalty( players[2]) * recovery_ratio

    return distance_rwd + sum(losses[p] for p in enemies) - loss_intensity * (sum(losses[p] for p in allies) + falling_loss)

def falling_penalty( player):
    if player[-1].pos_y > -9:
        return 0.0
    else :
        return abs(player[-1].pos_y) / 110.0




def distance(state):
    players = state.players
    x0 = players[0].pos_x
    y0 = players[0].pos_y
    x1 = players[2].pos_x
    y1 = players[2].pos_y

    dx = x1 - x0
    dy = y1 - y0

    return np.sqrt(np.square(dx) + np.square(dy))
