import p3.pad
import numpy as np
from .convert import convertState2Array
import p3.dqn
from p3.rewards import compute_rewards, isDone, isDead
import p3.state
from copy import deepcopy
import tensorflow.keras.losses
from tensorflow.keras.losses import categorical_crossentropy
import time
import threading

#TODO : rewards
#TODO : automate training
#TODO : additional techs
#TODO : delay actions


def loss(labels, logits):
    return categorical_crossentropy(labels, logits, from_logits=True)


tensorflow.keras.losses.loss = loss


class Fox:
    def __init__(self):
        self.action_list = []
        self.last_action = 0
        self.last_action_wait = 3
        self.reward_latency = 3
        self.n_episodes = 10000
        self.state_queue = []
        self.action_memory = []
        self.action_queue = [ -1 for _ in range(9)]
        self.rewards = [0]
        self.action_space = p3.pad.Action_Space()
        self.episode = 1
        self.life_span = 0
        self.last_action_id = -1
        self.reward_history = [np.nan for _ in range(20*60*60)]
        self.chosen_actions = np.array([0 for _ in range(self.action_space.len)])
        self.learning_session_length = 60*15  # 60 frames = 1sec -> session is 15 secs

        self.agent = p3.dqn.Agent( alpha=0.0001, gamma=0.98, n_actions=self.action_space.len, epsilon=1.0, batch_size=64,
                 input_dims=2*402, epsilon_dec=0.995,
                 epsilon_end=0.02, mem_size=60 * 60 * 5, fname="models\\model.h5", replace_target=60 * 60)
        try:
            pass
            # self.agent.load_model()
        except IOError:
            pass

    def init_state_queue(self, dummy):
        self.state_queue = [deepcopy(dummy) for _ in range(7)]

    def queue_state(self, state):
        self.state_queue.pop(0)
        self.state_queue.append(deepcopy(state))

    def queue_action(self, action_id):
        self.action_queue.pop(0)
        self.action_queue.append(action_id)

    def queue_reward(self, r):
        self.reward_history.pop(0)
        self.reward_history.append(r)

    def evaluate(self):
        r = compute_rewards(self.state_queue, life_span=self.life_span, loss_intensity=3, distance_ratio=0.003,
                               velocity_ratio=0., recovery_ratio=1.0, damage_ratio=0.01)

        return r

    def toNumpyArray(self):
        array = convertState2Array(self.state_queue, self.life_span, state_number=1)
        array = np.array([ value for sublist in array for value in sublist], dtype=np.float)
        #print("max_state:", np.max(array))
        return array

    def advance(self, state, pad):

        if state.frame-state.frame_start > 100 and (state.frame-state.frame_start) % self.learning_session_length == 0:

            print("Episode ", self.episode, ", avg episode reward=%.3f," % (np.mean(self.rewards)),
                  "avg last hour reward=%.3f," % (np.nanmean(self.reward_history)),
                  "epsilon=%.3f" % self.agent.epsilon)

            most = self.chosen_actions.argsort()[-5:][::-1]
            print("Most chosen actions:")
            for m in most:
                if isinstance(self.action_space[m], list):
                    print(m, self.chosen_actions[m], "action_chain", self.action_space[m][0])
                else:
                    print(m, self.chosen_actions[m], self.action_space[m])
            self.chosen_actions = np.array([0 for _ in range(self.action_space.len)]) # reset

            self.episode += 1
            self.rewards = []
            if self.episode == self.n_episodes+1:
                raise KeyboardInterrupt


        while len(self.action_memory) > 0 and state.frame - self.action_memory[0][0] >= self.reward_latency:
            reward = self.evaluate()
            # print(reward)
            self.rewards.append(reward*1000)
            self.queue_reward(reward*1000)
            self.agent.remember(self.action_memory[0][1], self.action_memory[0][2],
                                reward, self.toNumpyArray(), isDone(state))
            self.agent.learn()
            if isDead( state.players[2]):
                self.life_span = 0
            else:
                self.life_span += 1
            dumped = self.action_memory.pop(0)
            self.queue_action( dumped[2])

        while self.action_list:
            action = self.action_list[0]

            if state.frame - self.last_action < self.last_action_wait:
                return
            else:
                self.last_action_wait = action['duration']
                self.action_list.pop(0)
                action.send_controller( pad)
                self.last_action = state.frame
        else:
            """DECISION MAKING"""
            self.queue_state(state)
            #print("making choice")

            observation = self.toNumpyArray()
            banned = []
            # No down-b spam
            if 67 in [a for a in self.action_queue]:
                banned.append(67)
            if 66 in [a for a in self.action_queue]:
                banned.append(66)

            action_id = self.agent.choose_action(observation, strategy="epsilon_greedy", banned=banned)
            if action_id in banned:
                print("Used banned action!")

            self.chosen_actions[action_id] += 1
            action = self.action_space[action_id]

            self.action_memory.append([state.frame, observation, action_id])

            if isinstance(action, list):  # action chain
                for a in action:
                    self.action_list.append(
                        a
                    )
            else:
                self.action_list.append(
                    action
                )


    def shinespam(self, pad):
        self.action_list.append((0, pad.tilt_stick, [p3.pad.Stick.MAIN, 0.5, 0.0]))
        self.action_list.append((0, pad.press_button, [p3.pad.Button.B]))
        self.action_list.append((1, pad.release_button, [p3.pad.Button.B]))
        self.action_list.append((0, pad.tilt_stick, [p3.pad.Stick.MAIN, 0.5, 0.5]))
        self.action_list.append((0, pad.press_button, [p3.pad.Button.X]))
        self.action_list.append((1, pad.release_button, [p3.pad.Button.X]))
        self.action_list.append((1, None, []))
