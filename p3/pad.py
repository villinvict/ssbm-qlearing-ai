import enum
import os
import zmq
from _ctypes import *
import threading

@enum.unique
class UsefullButton(enum.Enum):
    A = 0
    B = 1
    X = 2
    Z = 4
    L = 6


@enum.unique
class Button(enum.Enum):
    A = 0
    B = 1
    X = 2
    Y = 3
    Z = 4
    START = 5
    L = 6
    R = 7
    D_UP = 8
    D_DOWN = 9
    D_LEFT = 10
    D_RIGHT = 11


@enum.unique
class Trigger(enum.Enum):
    L = 0
    R = 1

@enum.unique
class Stick(enum.Enum):
    MAIN = 0
    C = 1


class ControllerState(dict):
    def __init__(self):
        dict.__init__(self)
        for item in UsefullButton:
            self[item.name] = 0
        self['stick'] = (0.5, 0.5)
        self['c_stick'] = (0.5, 0.5)
        self['duration'] = 3

    def __str__(self):

        string = ""
        for item in UsefullButton:
            string += "%s(%d) " % (item.name, self[item.name])
        string += "S(%s,%s) " % self['stick']
        string += "C(%s,%s)" % self['c_stick']

        return string

        
    def make_state(self, button=None, stick=(0.5, 0.5), c_stick=(0.5, 0.5)):
        for item in UsefullButton:
            if button and item.name == button:
                self[button] = 1
            else:
                self[item.name] = 0

        self['stick'] = stick
        self['c_stick'] = c_stick


    def send_controller(self, pad):
        # info = []
        # for item in UsefullButton:
        #     info.append((item.name, self[item.name]))
        # info.append(self['stick'])
        # print(info)

        for item in UsefullButton:
            if self[item.name] == 0:
                pad.release_button( item, buffering=True)
            else:
                pad.press_button(item, buffering=True)
        x, y = self['stick']
        pad.tilt_stick(Stick.MAIN, x, y, buffering=True)
        x, y = self['c_stick']
        pad.tilt_stick(Stick.C, x, y, buffering=True)

        pad.flush()



class Action_Space(dict):
    def __init__(self):
        dict.__init__(self)
        self.stick_states = [
            (0.5, 0.5),
            (1.0, 0.5),
            (1.0, 0.0),
            (0.0, 0.0),
            (0.5, 0.0),
            (0.0, 0.5)
        ]

        self.stick_states_upB = [
            (1.0, 1.0),
            (0.5, 1.0),
            (0.0, 1.0)
        ]

        self.smash_states = [
            (0.5, 0.5),
            (0.5, 1.0),
            (0.5, 0.0),
            (0.0, 0.5),
            (1.0, 0.5)
        ]

        self.len = 0

        for s_state in self.stick_states:
            for item in UsefullButton:
                if not(item.name == 'B' and s_state[1] < 0.5):
                    c_state = ControllerState()
                    c_state.make_state(button=item.name, stick=s_state)
                    self[self.len] = c_state
                    self.len += 1

            for sc_state in self.smash_states:
                c_state = ControllerState()
                for item in UsefullButton:
                    c_state[item.name] = 0
                c_state['c_stick'] = sc_state
                c_state['stick'] = s_state
                self[self.len] = c_state
                self.len += 1

            # no button

            c_state = ControllerState()
            for item in UsefullButton:
                c_state[item.name] = 0
            c_state['c_stick'] = (0.5, 0.5)
            c_state['stick'] = s_state
            self[self.len] = c_state
            self.len += 1

        for s_state in self.stick_states_upB:
            c_state = ControllerState()
            c_state['B'] = 1
            for item in UsefullButton:
                if item.name != 'B':
                    c_state[item.name] = 0
            c_state['stick'] = s_state
            c_state['c_stick'] = (0.5, 0.5)
            self[self.len] = c_state
            self.len += 1

        # Specific techs:
        down_b_start = ControllerState()
        down_b_left = ControllerState()
        down_b_right = ControllerState()
        down_b_left_interrupt = ControllerState()
        down_b_right_interrupt = ControllerState()
        end = ControllerState()
        end['duration'] = 1
        down_b_start.make_state(button='B', stick=(0.5, 0.0))
        down_b_left.make_state(button='B', stick=(0.0, 0.5))
        down_b_right.make_state(button='B', stick=(1.0, 0.5))
        down_b_left_interrupt.make_state(stick=(0.0, 0.5))
        down_b_right_interrupt.make_state(stick=(1.0, 0.5))
        down_b_left['duration'] = 2
        down_b_right['duration'] = 2
        down_b_start['duration'] = 8
        down_b_left_interrupt['duration'] = 2
        down_b_right_interrupt['duration'] = 2

        perfect_down_b_left = [down_b_start] + [down_b_left_interrupt, down_b_left] * 10 + [end]

        self[self.len] = perfect_down_b_left
        self.len += 1
        
        perfect_down_b_right = [ down_b_start] + [down_b_right_interrupt, down_b_right] * 10 + [end]

        self[self.len] = perfect_down_b_right
        self.len += 1

        simple_jump_right = ControllerState()
        simple_jump_right.make_state(button='X', stick=(1.0, 0.5))
        simple_jump_left = ControllerState()
        simple_jump_left.make_state(button='X', stick=(0.0, 0.5))
        simple_jump_left['duration'] = 4
        simple_jump_right['duration'] = 4
        land_right = ControllerState()
        land_left = ControllerState()
        land_right.make_state(button='L', stick=(0.9, 0.3))
        land_left.make_state(button='L', stick=(0.1, 0.3))


        wd_right = [simple_jump_right, land_right]
        wd_left = [simple_jump_left, land_left]

        self[self.len] = wd_right
        self.len += 1
        self[self.len] = wd_left
        self.len += 1







class Pad:
    """Writes out controller inputs."""
    def __init__(self, path):
        """Create, but do not open the fifo."""
        self.pipe = None
        self.path = path
        self.context = zmq.Context()
        self.port = 5556
        self.message = ""
        self.action_space = []

    def __enter__(self):
        with open(self.path, 'w') as f:
            f.write(str(self.port))

        self.socket = self.context.socket(zmq.PUSH)
        self.socket.bind("tcp://127.0.0.1:%d" % self.port)

        return self

    def __exit__(self, *args):
        pass

    def flush(self):
        self.socket.send_string(self.message)
        self.message = ""

    def write(self, command, buffering=False):
        self.message += command + '\n'
        if not buffering:
            self.flush()

    def press_button(self, button, buffering=False):
        """Press a button."""
        assert button in Button or button in UsefullButton
        self.write('PRESS {}\n'.format(button.name), buffering)

    def release_button(self, button, buffering=False):
        """Release a button."""
        assert button in Button or button in UsefullButton
        self.write('RELEASE {}\n'.format(button.name), buffering)

    def press_trigger(self, trigger, amount, buffering=False):
        """Press a trigger. Amount is in [0, 1], with 0 as released."""
        assert trigger in Trigger
        assert 0 <= amount <= 1
        self.write('SET {} {:.2f}\n'.format(trigger.name, amount), buffering)

    def tilt_stick(self, stick, x, y, buffering=False):
        """Tilt a stick. x and y are in [0, 1], with 0.5 as neutral."""
        assert stick in Stick
        assert 0 <= x <= 1 and 0 <= y <= 1
        self.write('SET {} {:.2f} {:.2f}\n'.format(stick.name, x, y), buffering)


    def reset(self):
        for button in Button:
            self.release_button(button)
        for trigger in Trigger:
            self.press_trigger(trigger, 0)
        for stick in Stick:
            self.tilt_stick(stick, 0.5, 0.5)
