import numpy as np


def player2list(states, player_num, life_span=0):
    total = []
    for s in states:
        array = []
        p = s.players[player_num]
        # fields = [
            # p.action_frame,
            # [(1.0 if i == int(p.action_state.value) else 0) for i in range(383)],   # 383
            # p.attack_vel_x/3,
            # p.attack_vel_y/3,
            # [(1.0 if i == int(p.body_state.value) else 0) for i in range(3)],
            # [(1.0 if i == int(p.character.value) else 0) for i in range(25)],   # 26 but 26th unused
            # p.cursor_x/100,
            # p.cursor_y/100,
            # p.facing,
            # p.hitlag/6,     # 6 frames max
            # p.hitstun/100,  # 100 frames max
            # [(1.0 if i == int(p.jumps_used) else 0) for i in range(6)],
            # p.on_ground,
            # p.percent/1000,
            # p.pos_x/200,
            # p.pos_y/200,
            # p.self_air_vel_x/3,
            # p.self_air_vel_y/3,
            # p.speed_ground_x_self/3,
            # p.stocks,
            # p.type.value
            # ]
        fields = [
            p.action_frame/50.0,
            [(1.0 if i == int(p.action_state.value) else 0) for i in range(0x017E + 1)],
            p.attack_vel_x,
            p.attack_vel_y,
            [(1.0 if i == int(p.body_state.value) else 0) for i in range(3)],
            # p.character.value,
            p.cursor_x / 100.0,
            p.cursor_y / 100.0,
            p.facing,
            p.hitlag / 10.0,
            p.hitstun / 10.0,
            p.jumps_used,
            p.on_ground,
            p.percent / 100.0,
            p.pos_x / 100.0,
            p.pos_y / 100.0,
            p.self_air_vel_x / 2.0,
            p.self_air_vel_y / 2.0,
            p.speed_ground_x_self / 2.0,
        ]

        for f in fields:
            if isinstance(f, list):
                for ff in f:
                    array.append(float(ff))
            else:
                array.append(float(f))


        # print('player:', player_num, "cursor:", p.cursor_x, p.cursor_y, "hitlag:", p.hitlag, "jumps:", p.jumps_used)

        total.append(array)
    return total


def state_info2list(states):
    total = []
    for s in states:
        info = [
            # state.frame,
            # state.menu.value,
            # state.stage.value
            ]
        array = []
        for item in info:
            array.append(float(item))
        total.append(array)
    return total


def convertState2Array(state, life_span=0, state_number=6):
    return player2list(state[:state_number], 0) + player2list(state[-state_number:], life_span=life_span, player_num=2) + state_info2list(state)





