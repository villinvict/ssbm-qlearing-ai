from tensorflow.keras.layers import Dense, LayerNormalization
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.optimizers import Adam, Adadelta
from tensorflow.keras.backend import clear_session
from tensorflow.keras.losses import categorical_crossentropy
import math
import numpy as np
from copy import copy
import tempfile
import itertools as IT
import os
from threading import Thread
import pickle
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"


def loss(labels, logits):
    return categorical_crossentropy(labels, logits, from_logits=True)


def uniquify(path, sep=''):
    def name_sequence():
        count = IT.count()
        yield ''
        while True:
            yield '{s}{n:d}'.format(s=sep, n=next(count))
    orig = tempfile._name_sequence
    with tempfile._once_lock:
        tempfile._name_sequence = name_sequence()
        path = os.path.normpath(path)
        dirname, basename = os.path.split(path)
        filename, ext = os.path.splitext(basename)
        fd, filename = tempfile.mkstemp(dir=dirname, prefix=filename, suffix=ext)
        tempfile._name_sequence = orig
    return filename

class ReplayBuffer(object):
    def __init__(self, max_size, input_shape, n_actions,
                 discrete=False, file="exp.csv"):
        self.mem_size = max_size
        self.mem_cntr = 0
        self.discrete = discrete
        self.file = file
        self.state_memory = np.zeros((self.mem_size, input_shape))
        self.new_state_memory = np.zeros((self.mem_size, input_shape))
        dtype = np.int8 if self.discrete else np.float32
        self.action_memory = np.zeros((self.mem_size, n_actions), dtype=dtype)
        self.reward_memory = np.zeros(self.mem_size)
        self.terminal_memory = np.zeros(self.mem_size, dtype=np.float32)

    def store_transition(self, state, action, reward, state_, done):
        index = self.mem_cntr % self.mem_size
        self.state_memory[index] = state
        self.new_state_memory[index] = state_
        if self.discrete:
            actions = np.zeros(self.action_memory.shape[1])
            actions[action] = 1.0
            self.action_memory[index] = actions
        else:
            self.action_memory[index] = action
        self.reward_memory[index] = reward
        self.terminal_memory[index] = 1 - int(done)
        self.mem_cntr += 1

    def sample_buffer(self, batch_size):
        max_mem = min(self.mem_cntr, self.mem_size)
        batch = np.random.choice(max_mem, batch_size)
        states = self.state_memory[batch]
        new_states = self.new_state_memory[batch]
        actions = self.action_memory[batch]
        rewards = self.reward_memory[batch]
        terminal = self.terminal_memory[batch]

        return states, actions, rewards, new_states, terminal

    def load(self):
        try:
            print("Loading memory data")
            with open(self.file, "rb") as fp:
                full_data = pickle.load(fp)

            self.state_memory = full_data[0]
            self.new_state_memory = full_data[1]
            self.action_memory = full_data[2]
            self.reward_memory = full_data[3]
            self.terminal_memory = full_data[4]
            self.mem_cntr = full_data[5]
            self.mem_size = full_data[6]
        except Exception:
            print("Can't find memory file..")
            pass

    def save(self):
        try:
            full_data = list()
            full_data.append(self.state_memory)
            full_data.append(self.new_state_memory)
            full_data.append(self.action_memory)
            full_data.append(self.reward_memory)
            full_data.append(self.terminal_memory)
            full_data.append(self.mem_cntr)
            full_data.append(self.mem_size)

            print("...Saving Memory...")
            with open(self.file, "wb") as fp:
                pickle.dump(full_data, fp)

            print("Memory Data saved at", self.file)

        except Exception:
            print("Error while saving memory")
            pass

def build_dqn(alpha, n_actions, input_dims, fc_dims):
    model = Sequential([
        Dense(fc_dims, input_shape=(input_dims,), activation='elu'),
        Dense(fc_dims, activation='elu'),
        Dense(fc_dims, activation='elu'),
        Dense(fc_dims, activation='elu'),
        Dense(fc_dims, activation='elu'),
        Dense(n_actions, activation='elu'),
        # Dense(n_actions, activation='softmax'),
    ])

    model.compile(optimizer=Adadelta(lr=alpha),
                  loss='mse')

    return model


class Agent(object):
    def __init__(self, alpha, gamma, n_actions, epsilon, batch_size,
                 input_dims, epsilon_dec=0.996,
                 epsilon_end=0.17, mem_size=60 * 60 * 3, fname='model.h5', replace_target=100, save_memory = False):
        self.n_actions = n_actions
        self.action_space = [i for i in range(self.n_actions)]
        self.gamma = gamma
        self.alpha = alpha
        self.epsilon = epsilon
        self.epsilon_dec = epsilon_dec
        self.epsilon_min = epsilon_end
        self.batch_size = batch_size
        self.model_file = fname
        self.action_history = []
        self.replace_target = replace_target
        self.memory = ReplayBuffer(mem_size, input_dims, n_actions, True, file='models\\memory')
        self.q_eval = build_dqn(self.alpha, n_actions, input_dims, 256)
        self.q_target = build_dqn(self.alpha, n_actions, input_dims, 256)
        self.learn_cntr = 1
        self.save_memory = save_memory

        self.generation = 0
        self.cycle_reward = 0
        self.isBestPlayer = False

        self.update_network_parameters()

    def remember(self, state, action, reward, new_state, done):
        self.memory.store_transition(state, action, reward, new_state, done)

    def choose_action(self, state, strategy='boltzmann', banned=[]):
        state = state[np.newaxis, :]
        rand = np.random.random()
        Q = list(self.q_eval.predict_on_batch(state).numpy()[0])
        action_space = copy(self.action_space)
        for ban in banned:
            action_space.pop(ban)


        if strategy == "boltzmann":
            total = sum([math.exp(val / self.epsilon) for val in Q])
            probs = [math.exp(val / self.epsilon) / total for val in Q]


            cumulative_prob = 0.0
            for i in range(len(probs)):
                cumulative_prob += probs[i]
                if (cumulative_prob > rand):
                    return i
            return np.argmax(probs)
        else:

            if rand < self.epsilon:
                action = np.random.choice(action_space)
                # print("Random action")
            else:
                for ban in banned:
                    Q[ban] = -np.inf
                action = np.argmax(Q)
                # print("maxQ", np.max(Q))

        return action

    def learn(self, retrain=False):
        if self.memory.mem_cntr > self.batch_size:
            if retrain:
                batch_size = min([self.memory.mem_cntr, retrain])
            else:
                batch_size = self.batch_size

            state, action, reward, new_state, done = \
                self.memory.sample_buffer(batch_size)
            action_values = np.array(self.action_space, dtype=np.int8)
            action_indices = np.dot(action, action_values)

            q_next = np.array(self.q_target.predict_on_batch(new_state))
            q_eval = np.array(self.q_eval.predict_on_batch(new_state))
            q_pred = np.array(self.q_eval.predict_on_batch(state))

            max_actions = np.argmax(q_eval, axis=1)

            q_target = q_pred

            batch_index = np.arange(batch_size, dtype=np.int32)
            q_target[batch_index, action_indices] = reward + \
                self.gamma*q_next[batch_index, max_actions.astype(int)]*done

            _ = self.q_eval.train_on_batch(state, q_target)

            if not retrain:

                self.epsilon = self.epsilon * self.epsilon_dec if self.epsilon > \
                    self.epsilon_min else self.epsilon_min

                if self.learn_cntr % self.replace_target == 0:
                    self.update_network_parameters()
                self.learn_cntr += 1

            clear_session()

    def learn_async(self):

        t = Thread( target=self.learn)
        t.start()

    def reset_weights(self, reset_probability):

        i = 0
        weights = np.array(self.q_eval.get_weights())

        for enum, layer in enumerate(weights):
            for enum2, layer2 in enumerate(layer):
                if isinstance(layer2, np.ndarray):
                    for index, value in enumerate(layer2):
                        if np.random.random() < reset_probability:
                            weights[enum][enum2][index] = np.random.uniform(-1, 1)
                            i += 1
        print('reset', i, 'weights.')
        self.q_eval.set_weights(weights)
        self.update_network_parameters()

    def update_network_parameters(self):
        self.q_target.set_weights(self.q_eval.get_weights())

    def copy_network_parameters(self, agent):
        self.q_target.set_weights(agent.q_target.get_weights())
        self.q_eval.set_weights(agent.q_eval.get_weights())
        self.epsilon = agent.epsilon

    def copy_memory(self, agent):
        self.memory = copy(agent.memory)

    def save_model(self):
        print('...Saving model...')

        self.q_eval.save(uniquify(self.model_file))
        if self.save_memory:
            self.memory.save()

    def load_model(self):
        print('...Loading model...')
        self.q_eval = load_model(self.model_file, custom_objects={'loss': loss})
        self.update_network_parameters()
        self.epsilon = self.epsilon_min
        if self.save_memory:
            self.memory.load()



