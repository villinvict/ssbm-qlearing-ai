from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.backend import clear_session
import numpy as np
import zmq

from Memory import ReplayBuffer
from dqn_builder import build_dqn
from utils import uniquify
from Plot import Plot


class Learner():
    def __init__(self, alpha, gamma, n_actions, epsilon, batch_size, port, n_actors,
                 input_dims, epsilon_dec=0.996, loss='mse', layer_size=256, epsilon_end=0.17,
                 mem_size=60 * 60 * 3, fname='model.h5', replace_target=100, n_loops=10*5, request_check_freq=1,
                 learn_freq=10, plot_path="out.png"):

        self.n_loops = n_loops
        self.n_actions = n_actions
        self.n_actors = n_actors
        self.port = port
        self.action_space = [i for i in range(self.n_actions)]
        self.gamma = gamma
        self.alpha = alpha
        self.loss = loss
        self.epsilon = epsilon
        self.epsilon_dec = epsilon_dec
        self.epsilon_min = epsilon_end
        self.batch_size = batch_size
        self.model_file = fname
        self.replace_target = replace_target
        self.request_check_freq = request_check_freq
        self.learn_freq = learn_freq
        self.plot = Plot( plot_path)

        self.memory = ReplayBuffer(mem_size, input_dims, n_actions, True)
        self.q_eval = build_dqn(n_actions, input_dims, layer_size)
        self.q_target = build_dqn(n_actions, input_dims, layer_size)
        self.compile_model()

        self.learn_cntr = 1

        self.update_network_parameters()
        self.init_pipe()

    def compile_model(self):
        self.q_eval.compile(optimizer=Adam(lr=self.alpha), loss=self.loss)
        self.q_target.compile(optimizer=Adam(lr=self.alpha), loss=self.loss)

    def init_pipe(self):
        # REP ( get experience and send Weights)
        context = zmq.Context()
        self.socket = context.socket( zmq.REP)
        self.socket.bind("tcp://127.0.0.1:%d" % self.port)

    def get_requests(self):
        # Try to get some experience
        try:
            exp, score = self.socket.recv_pyobj( zmq.NOBLOCK)
            score *= 1000
            print('received', len(exp), 'experiences | score =%.3f' % score)
            for e in exp:
                self.remember(*e)
            self.plot.update( score)

            # and send the model's weights
            self.socket.send_pyobj((self.q_eval.get_weights(), self.epsilon))

        except zmq.error.Again:
            # print('no request received')
            # Send the model state
            pass

    def remember(self, state, action, reward, new_state, done):
        self.memory.store_transition(state, action, reward, new_state, done)

    def learn(self):
        if self.memory.mem_cntr > self.batch_size:
            batch_size = self.batch_size

            state, action, reward, new_state, done = \
                self.memory.sample_buffer(batch_size)
            action_values = np.array(self.action_space, dtype=np.int8)
            action_indices = np.dot(action, action_values)

            q_next = np.array(self.q_target.predict_on_batch(new_state))
            q_eval = np.array(self.q_eval.predict_on_batch(new_state))
            q_pred = np.array(self.q_eval.predict_on_batch(state))

            max_actions = np.argmax(q_eval, axis=1)

            q_target = q_pred

            batch_index = np.arange(batch_size, dtype=np.int32)
            q_target[batch_index, action_indices] = reward + \
                                                    self.gamma * q_next[batch_index, max_actions.astype(int)] * done

            _ = self.q_eval.train_on_batch(state, q_target)

            self.epsilon = self.epsilon * self.epsilon_dec if self.epsilon > \
                                                              self.epsilon_min else self.epsilon_min

            if self.learn_cntr % self.replace_target == 0:
                self.update_network_parameters()
            self.learn_cntr += 1

            clear_session()

    def update_network_parameters(self):
        self.q_target.set_weights(self.q_eval.get_weights())

    def copy_network_parameters(self, agent):
        self.q_target.set_weights(agent.q_target.get_weights())
        self.q_eval.set_weights(agent.q_eval.get_weights())
        self.epsilon = agent.epsilon

    def save_model(self):
        print('...Saving model...')
        self.q_eval.save(uniquify(self.model_file))

    def load_model(self):
        print('...Loading model...')
        self.q_eval = load_model(self.model_file)
        self.update_network_parameters()
        self.epsilon = self.epsilon_min

    def run(self):
        try:
            cntr = 1
            while True:
                # Learn, and accept requests :)
                if cntr % self.learn_freq == 0:
                    self.learn()
                if cntr % self.request_check_freq == 0:
                    self.get_requests()
                cntr += 1
                if cntr == self.n_loops:
                    raise KeyboardInterrupt
        except KeyboardInterrupt:
                pass

        self.plot.save()
        self.save_model()
        print("Learner Exited")
