from dqn_builder import build_dqn
from DolphinInitializer import DolphinInitializer
from MenuManager import MenuManager
import State
import Pad
import zmq
import numpy as np
from copy import copy
import time
from threading import Thread, Event, active_count

from utils import convertState2Array
from rewards import compute_rewards
from copy import deepcopy


class Actor(Thread):
    def __init__(self, layer_size, dolphin_exe, iso_path, video_backend,
                 mw, sm, pad, state, input_dims, port, actor_id, send_exp_freq=60*5):

        Thread.__init__(self)
        self.shutdown_flag = Event()

        self.dolphin_thread = DolphinInitializer(  self, dolphin_exe, iso_path, video_backend)
        self.id = actor_id
        self.port = port
        self.epsilon = 1.0
        self.action_space = Pad.Action_Space()
        self.discrete_action_space = [ i for i in range(self.action_space.len)]
        self.nn = build_dqn(self.action_space.len, input_dims, layer_size)
        self.exp = []
        self.send_exp_freq = send_exp_freq
        self.game_start = True

        self.mw = mw
        self.sm = sm
        self.pad = pad
        self.state = state
        self.mm = MenuManager()

        self.action_list = []
        self.last_action = 0
        self.last_action_wait = 3
        self.reward_latency = 10
        self.status_detect_freq = 60 + 15
        self.state_queue = []
        self.action_memory = []
        self.status_hist = [0, 0, 0, 0]
        self.learning_session_length = 60 * 15  # 60 frames = 1sec -> session is 15 secs
        self.init_pipe()

        self.init_state_queue( state)

    def init_pipe(self):
        # REQ send experience and get Weights)
        context = zmq.Context()
        self.socket = context.socket(zmq.REQ)
        self.socket.connect("tcp://127.0.0.1:%d" % self.port)

        print("Initialized Learner-Actor connection at tcp://127.0.0.1:%d" % self.port)

    def init_state_queue(self, dummy):
        self.state_queue = [deepcopy(dummy) for _ in range(5)]

    def queue_state(self, players=[0, 1]):
        self.state_queue.pop(0)
        self.state_queue.append(deepcopy(self.state))

        # Update status history
        for p_num in players:
            self.update_status_hist(p_num)

    def update_status_hist(self, p_num):
        if self.status_hist[p_num] > 0:
            self.status_hist[p_num] -= 1

    def isDead(self, p_num):
        if self.status_hist[p_num] == 0:
            for state in self.state_queue:
                if state.players[p_num].action_state.value <= 0xA:
                    print('player', p_num, 'is dead.')
                    if state.players[p_num].action_state.value == 0x4:
                        self.status_hist[p_num] = self.status_detect_freq * 3
                    else:
                        self.status_hist[p_num] = self.status_detect_freq
                    return True
        return False

    def isRespawning(self, p_num):
        if self.status_hist[p_num] == 0:
            for state in self.state_queue:
                if state.players[p_num].action_state.value <= 0x0C:
                    self.status_hist[p_num] = self.status_detect_freq
                    return True
        return False

    def isDone(self, players=[0, 1]):
        for p_num in players:
            if self.isRespawning(p_num):
                print('Done.')
                return True

        return False

    def evaluate(self, death_loss=1.0):
        r = int(self.isDead(0)) - death_loss * int(self.isDead(1))  # Death

        r += compute_rewards(self.state_queue, loss_intensity=1, distance_ratio=0.,
                             velocity_ratio=0., recovery_ratio=0., damage_ratio=0.01, )
        return r

    def toNumpyArray(self):
        array = convertState2Array(self.state_queue, state_number=1)
        array = np.array([value for sublist in array for value in sublist], dtype=np.float)
        return array

    def choose_action(self, state):
        state = state[np.newaxis, :]
        rand = np.random.random()
        Q = self.nn.predict_on_batch(state)
        if rand < self.epsilon:
            action = np.random.choice(self.discrete_action_space)
            # print("Random action")
        else:
            action = np.argmax(Q)
            # print("maxQ", np.max(Q))

        return action

    def store_exp(self):
        while len(self.action_memory) > 0 and self.state.frame - self.action_memory[0][0] >= self.reward_latency + \
                self.action_memory[0][3]:

            self.exp.append((self.action_memory[0][1], self.action_memory[0][2],
                             self.evaluate(death_loss=1.0), self.toNumpyArray(), self.isDone()))

            self.action_memory.pop(0)

    def get_score(self):
        r = 0.0
        for e in self.exp:
            r += e[2]

        return r/len(self.exp)

    def send_data(self):
        # print(self.exp)
        if len(self.exp) > 0:
            # Send experience collected
            self.socket.send_pyobj((self.exp, self.get_score()))
            weights, epsilon = self.socket.recv_pyobj()

            # Update the nn
            self.nn.set_weights(weights)
            self.epsilon = epsilon
            self.exp = []

    def data_sender_factory(self):

        return Thread( target=self.send_data)


    def advance(self):
        self.queue_state()

        if not self.action_list:
            if self.state.frame - self.last_action >= self.last_action_wait:
                """DECISION MAKING"""

                observation = self.toNumpyArray()
                action_id = self.choose_action(observation)

                action = self.action_space[action_id]

                if isinstance(action, list):  # action chain
                    duration = 0
                    for a in action:
                        duration += a['duration']
                        self.action_list.append(
                            a
                        )
                    self.action_memory.append([self.state.frame, observation, action_id, duration])
                else:
                    self.action_memory.append([self.state.frame, observation, action_id, action['duration']])
                    self.action_list.append(
                        action
                    )

        if self.action_list and self.state.frame - self.last_action >= self.last_action_wait:
            action = self.action_list[0]

            self.last_action_wait = action['duration']
            self.action_list.pop(0)
            # ban dodges when recovering
            if (not self.state.players[1].on_ground) and abs(self.state.players[1].pos_x) > 59 and action['L'] == 1:
                Pad.neutralPad.send_controller(self.pad)
            else:
                action.send_controller(self.pad)
            self.last_action = self.state.frame

    def make_action(self):
        if self.state.menu == State.Menu.Game:
            if self.game_start:
                self.game_start = False
                self.state.frame_start = self.state.frame
                time.sleep(2)

            self.advance()

        elif self.state.menu == State.Menu.Characters:
            self.mm.pick_char(self.state, self.pad)
        elif self.state.menu == State.Menu.Stages:
            # Handle this once we know where the cursor position is in memory.
            self.pad.tilt_stick(Pad.Stick.C, 0.5, 0.5)
        elif self.state.menu == State.Menu.PostGame:
            self.mm.press_start_lots(self.state, self.pad)

    def main_loop(self):

        last_frame = self.state.frame
        res = next(self.mw)
        if res is not None:
            self.sm.handle(res)

        if self.state.frame > last_frame:
            if self.state.frame - last_frame > 1:
                print('Actor', self.id, 'skipped', self.state.frame - last_frame, 'frames.')
            self.make_action()

        self.mw.advance()
        self.store_exp()

    def run(self):
        try:
            cntr = 1
            # Start bounded dolphin process
            self.dolphin_thread.start()
            # wait dolphin
            time.sleep(5)
            while not self.shutdown_flag.is_set():
                self.main_loop()
                if cntr % self.send_exp_freq == 0:
                    t = time.time()
                    sender = self.data_sender_factory()
                    sender.start()
                    dt = time.time() - t
                    #print("time spent on sending data:%.3f" % dt)
                    n_threads = active_count()
                    if n_threads > 5:
                        print('active threads:', n_threads)
                cntr += 1
        except KeyboardInterrupt:
            pass

        self.dolphin_thread.shutdown_flag.set()
        self.dolphin_thread.join()

        print("Actor", self.id, "Exited")

