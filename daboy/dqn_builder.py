from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense


def build_dqn(n_actions, input_dims, fc_dims):
    model = Sequential([
        Dense(fc_dims, input_shape=(input_dims,), activation='elu'),
        Dense(fc_dims, activation='elu'),
        Dense(fc_dims, activation='elu'),
        Dense(fc_dims, activation='elu'),
        Dense(fc_dims, activation='elu'),
        Dense(n_actions, activation='elu'),
    ])
    return model
