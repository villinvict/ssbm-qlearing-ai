import numpy as np

def compute_damages(player):
  return float(max([player[-1].percent - player[0].percent, 0]))

def compute_rewards(states, enemies=[0], allies=[1], damage_ratio=0.01, distance_ratio=0.0005, loss_intensity=1.0,
                    velocity_ratio=0.003, recovery_ratio=0.1):
    """Computes rewards from a list of state transitions.

    Args:
      states: current states
      enemies: The list of pids on the enemy team.
      allies: The list of pids on our team.
      damage_ratio: How much damage (percent) counts relative to stocks.
      distance_ratio: How much distance counts
      loss_intensity: does loosing suck ?
      velocity_ratio: make if flashy
      recovery_ratio: reward if goes up when low on y axis
    Returns:
      A length T numpy array with the rewards on each transition.
    """

    pids = enemies + allies

    players = [ [], [], [], []]
    for enum, _ in enumerate(players):
        if enum in pids:
            players[enum] = [state.players[enum] for state in states]

    # players = states['players'] -70.3 -11.75 -110

    # print(players[0][-1].pos_y)

    damages = {p: compute_damages(players[p]) for p in pids}
    losses = {p:  damage_ratio * damages[p] for p in pids}
    distance_rwd = (distance(states[0]) - distance(states[-1])) * distance_ratio
    # falling_loss = falling_penalty( players[1]) * recovery_ratio

    return distance_rwd + sum(losses[p] for p in enemies) - loss_intensity * (sum(losses[p] for p in allies))


def falling_penalty( player):
    if player[-1].pos_y > -9:
        return 0.0
    else:
        # print("Has to recover !")
        return abs(player[-1].pos_y) / 110.0


def distance(state):
    players = state.players
    x0 = players[0].pos_x
    y0 = players[0].pos_y
    x1 = players[1].pos_x
    y1 = players[1].pos_y

    dx = x1 - x0
    dy = y1 - y0

    return np.sqrt(np.square(dx) + np.square(dy))
