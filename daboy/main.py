from Default import Default, Option
from Actor import Actor
from Learner import Learner
from State import State
from StateManager import StateManager
from MemoryWatcher import MemoryWatcher
from Pad import Pad

import os
import psutil
from time import sleep
from argparse import ArgumentParser
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"


class Main( Default):
    _options = [
        Option('learning_rate', type=float, default=0.0001),
        Option('discount_factor', type=float, default=0.99),
        Option('batch_size', type=int, default=256),
        Option('file_name', type=str, default='models\\model.h5', help="path to which the checkpoint will be loaded/saved"),
        Option('optimizer', type=str, default="GradientDescent", help="which tf optimizer to use"),
        Option('mem_size', type=int, default=10**6, help='amount of experience stockable'),
        Option('n_actors', type=int, default=1, help='number of actors'),
        Option('replace_target', type=int, default=10**5, help='frequency at which the target q_value is updated'),
        Option('load_checkpoint', type=bool, default=False),
        Option('learner_port', type=int, default=5557),
        Option('mw_port', type=int, default=5555),  # Has to work with multiple actors
        Option('pad_port', type=int, default=5556),
        Option('loss', type=str, default='mse'),
        Option('layer_size', type=int, default=256),
        Option('dolphin_dir', type=str, default=r'C:\Users\Victor\Documents\Dolphin Emulator'),
        Option('dolphin_exe', type=str, default=r'C:\Users\Victor\Desktop\dolphin-mw\Binary\x64\Dolphin.exe'),
        Option('iso_path', type=str, default=r'C:\Users\Victor\Desktop\GRAND BAZARD\ssbmja\ssbm.iso'),
        Option('video_backend', type=str, default='OGL'),
        Option('n_loops', type=int, default=10*5)
    ]

    _members = [
    ]

    def __init__(self, **kwargs):
        Default.__init__(self, init_members=False, **kwargs)

        input_shape = 802 # automate
        n_actions = 37      # automate
        pad_path = self.dolphin_dir + r"\Pipes\daboy"
        mw_path = self.dolphin_dir + r"\MemoryWatcher\MemoryWatcher"



        # alpha, gamma, n_actions, epsilon, batch_size, port, n_actors,
        #                  input_dims, epsilon_dec=0.996, loss='mse', layer_size=256,
        #                  epsilon_end=0.17, mem_size=60 * 60 * 3, fname='model.h5', replace_target=100, save_memory=False

        self.learner = Learner(  self.learning_rate, self.discount_factor, n_actions, 1.0, self.batch_size,
                                 self.learner_port, self.n_actors, input_shape, epsilon_dec=0.996, loss=self.loss,
                                 layer_size=self.layer_size, epsilon_end=0.01, mem_size=self.mem_size,
                                 fname=self.file_name, replace_target=self.replace_target, n_loops=self.n_loops)
        self.learner.q_eval.summary()

        if self.load_checkpoint:
            try:
                self.learner.load_model()
            except Exception as e:
                print('Error loading checkpoint:', e)

        self.states = [State() for _ in range(self.n_actors)]

        self.actors = [
            Actor(
                self.layer_size, self.dolphin_exe, self.iso_path, self.video_backend, MemoryWatcher( mw_path, self.mw_port),
                StateManager( self.states[i]), Pad( pad_path, self.pad_port), self.states[i], input_shape,
                self.learner_port, i, 60*2) for i in range( self.n_actors)
        ]

        self.write_locations()

    def write_locations(self):
        for actor in self.actors:
            locations = actor.sm.locations()
            """Writes out the locations list to the appropriate place under dolphin_dir."""
            path = self.dolphin_dir + r'\MemoryWatcher\Locations.txt'
            with open(path, 'w') as f:
                f.write('\n'.join(locations))

    def __call__(self):
        """main loop"""
        # Boot actors
        for actor in self.actors:
            actor.start()

        # Wait for SIGINT
        # --------------------------- #
        self.learner.run()
        # --------------------------- #

    def exit(self):
        print('...Exiting daboy...')

        # Interruption actors
        for actor in self.actors:
            actor.shutdown_flag.set()

        # Wait all the threads to end
        for actor in self.actors:
            actor.join()

        print('Exited')




if __name__ == '__main__':
    proc = psutil.Process(os.getpid())

    parser = ArgumentParser()
    for opt in Main.full_opts():
        opt.update_parser(parser)
    args = parser.parse_args()

    main = Main( **args.__dict__)

    main()

    main.exit()





