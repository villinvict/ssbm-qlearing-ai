import matplotlib
matplotlib.use('Pdf')

import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import savgol_filter


class Plot:

    def __init__(self, path, smoothing=100):
        self.path = path
        self.smoothing = smoothing
        self.y = []

    def update(self, y):
        self.y.append(y)

    def save(self):
        y_smooth = self.smooth()
        x = [i for i in range(len(y_smooth))]
        plt.plot(x, y_smooth)
        plt.savefig(self.path)

    def smooth(self):
        smoothed = []
        ys = [self.y[i] for i in range(len(self.y)) if i % 10 == 0]
        for i in range(len(ys)):
            min_offset = -min(i, self.smoothing)
            max_offset = min(len(self.y)-i-1, self.smoothing)
            smoothed.append( np.mean([self.y[i+offset] for offset in range(min_offset, max_offset)]))

        return smoothed
