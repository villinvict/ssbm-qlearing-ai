import itertools as IT
import tempfile
import os


def uniquify(path, sep=''):
    def name_sequence():
        count = IT.count()
        yield ''
        while True:
            yield '{s}{n:d}'.format(s=sep, n=next(count))
    orig = tempfile._name_sequence
    with tempfile._once_lock:
        tempfile._name_sequence = name_sequence()
        path = os.path.normpath(path)
        dirname, basename = os.path.split(path)
        filename, ext = os.path.splitext(basename)
        fd, filename = tempfile.mkstemp(dir=dirname, prefix=filename, suffix=ext)
        tempfile._name_sequence = orig
    return filename


def player2list(states, player_num):
    total = []
    for s in states:
        array = []
        p = s.players[player_num]

        fields = [
            p.action_frame/50.0,
            [(1.0 if i == int(p.action_state.value) else 0) for i in range(0x017E + 1)],
            p.attack_vel_x,
            p.attack_vel_y,
            [(1.0 if i == int(p.body_state.value) else 0) for i in range(3)],
            # p.character.value,
            p.facing,
            p.hitlag / 10.0,
            p.hitstun / 10.0,
            p.jumps_used,
            p.on_ground,
            p.shield_size/100,
            p.percent / 100.0,
            p.pos_x / 20.0,
            p.pos_y / 20.0,
            p.self_air_vel_x / 2.0,
            p.self_air_vel_y / 2.0,
            p.speed_ground_x_self / 2.0,
        ]

        for f in fields:
            if isinstance(f, list):
                for ff in f:
                    array.append(float(ff))
            else:
                array.append(float(f))

        total.append(array)
    return total


def state_info2list(states):
    total = []
    for s in states:
        info = [
            # state.frame,
            # state.menu.value,
            # state.stage.value
            ]
        array = []
        for item in info:
            array.append(float(item))
        total.append(array)
    return total


def convertState2Array(state, state_number=1):
    return player2list(state[-state_number:], player_num=0) + player2list(state[-state_number:], player_num=1) + state_info2list(state)





