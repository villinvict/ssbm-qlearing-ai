from threading import Thread, Event
from subprocess import Popen
from time import sleep
import signal


class DolphinInitializer(Thread):

    def __init__(self, actor, exe, iso_path, video):
        Thread.__init__(self)
        self.shutdown_flag = Event()
        self.actor = actor
        self.exe = exe
        self.iso_path = iso_path
        self.video = video

    def run(self):

        cmd = r'%s -b --exec="%s" --video_backend="%s"' \
              % ( self.exe, self.iso_path, self.video)

        print('dolphin', self.actor.id, 'started.')
        proc = Popen( cmd)

        while not self.shutdown_flag.is_set():
            sleep(3)

        proc.send_signal( signal.SIGTERM)
        print('sent signal to Dolphin process', self.actor.id)





