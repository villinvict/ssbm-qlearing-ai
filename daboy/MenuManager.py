import math
import Pad
from time import sleep

characters = dict(
  fox=(-23.5, 11.5),
  falco=(-30, 11),
  falcon=(18, 18),
  roy=(18, 5),
  marth=(11, 5),
  zelda=(11, 11),
  sheik=(11, 11),
  mewtwo=(-2, 5),
  luigi=(-16, 18),
  mario=(-23.5, 14.5),
  puff=(-10, 5),
  kirby=(-2, 11),
  peach=(-2, 18),
  ganon=(23, 18),
  samus=(3, 11),
  bowser=(-9, 19),
  yoshi=(5, 18),
  dk=(11, 18),
)

settings = (0, 24)


class MenuManager:
    def __init__(self):
        self.selected = False

    def pick_char(self, state, pad, char='mario'):
        if self.selected:
            # Release buttons and lazily rotate the c stick.
            pad.release_button(Pad.Button.A)
            pad.tilt_stick(Pad.Stick.MAIN, 0.5, 0.5)
            angle = (state.frame % 240) / 240.0 * 2 * math.pi
            pad.tilt_stick(Pad.Stick.C, 0.4 * math.cos(angle) + 0.5, 0.4 * math.sin(angle) + 0.5)
        else:
            target_x, target_y = characters[char]
            dx = target_x - state.players[1].cursor_x
            dy = target_y - state.players[1].cursor_y
            mag = math.sqrt(dx * dx + dy * dy)
            if mag < 0.3:
                pad.press_button(Pad.Button.A)
                pad.press_button(Pad.Button.A)
                self.selected = True
            else:
                pad.tilt_stick(Pad.Stick.MAIN, 0.5 * (dx / mag) + 0.5, 0.5 * (dy / mag) + 0.5)

    def press_start_lots(self, state, pad):
        if state.frame % 2 == 0:
            pad.press_button(Pad.Button.START)
        else:
            pad.release_button(Pad.Button.START)
